#!/bin/env python
# vim: textwidth=0:
import sys
from collections import deque

# Represent the HMM from figure 6.3 in python
states = frozenset(['hot', 'cold'])
observations = frozenset(['1', '2', '3'])
observation_likelihoods = {
    'hot': { '1': 0.2, '2': 0.4, '3': 0.4 },
    'cold': { '1': 0.5, '2': 0.4, '3': 0.1}
}

transition_probabilities = {
    'hot': {
        'hot': 0.7,
        'cold': 0.3
    },
    'cold': {
        'hot': 0.4,
        'cold': 0.6
    }
}

initial_probabilities = {
    'hot': 0.8,
    'cold': 0.2
}

# Viterbi algorithm
def viterbi(obs):
    # Construct the probability matrix and backpointer table
    p_mat = {}
    backpointer = {}

    for s in states:
        p_mat[s] = []
        backpointer[s] = []

        for i in range(len(obs)):
            p_mat[s].append(None)
            backpointer[s].append(None)

    # Initialization step
    for s in states:
        p_mat[s][0] = initial_probabilities[s] * observation_likelihoods[s][obs[0]]
        backpointer[s][0] = None

    # Recursion step
    for t in range(1, len(obs)):
        for s in states:

            # Determine the most probable state to transition to
            max_prob = None
            max_state = None

            for s1 in states:
                prob = p_mat[s1][t-1] * transition_probabilities[s1][s] * observation_likelihoods[s][obs[t]]

                if (max_prob == None and max_state == None) or prob > max_prob:
                    max_prob = prob
                    max_state = s1

            # Set the newly found probabilities and states
            p_mat[s][t] = max_prob
            backpointer[s][t] = max_state

    # Termination step
    max_prob = None
    max_state = None

    for s in states:
        prob = p_mat[s][len(obs) - 1]

        if (max_prob == None and max_state == None) or prob > max_prob:
            max_prob = prob
            max_state = s

    # Determine the path
    path = deque([max_state])
    for t in reversed(range(1, len(obs))):
        path.appendleft(backpointer[path[0]][t])

    return (max_prob, path)

# CLI program
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: viterbi.py <observed sequence>"
        exit(1)

    arg = sys.argv[1]
    observed = []

    for c in arg:
        observed.append(c)

        if c not in observations:
            print "Given observed sequence contains a value not in the HMM"
            print "Valid characters: ", ', '.join(sorted(observations))
            exit(2)

    (prob, path) = viterbi(observed)

    print "Most probable hidden state sequence: "
    print ' -> '.join(path), '\n'
    print "With a probability of: ", prob

